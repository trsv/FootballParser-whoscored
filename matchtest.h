#ifndef MATCHTEST_H
#define MATCHTEST_H

#include "match.h"

#include <QObject>
#include <QtTest/QtTest>

/**
 * @brief The MatchTest class - набор тестов для матча
 */
class MatchTest : public QObject
{
    Q_OBJECT

public:
    explicit MatchTest(QObject *parent = nullptr);

signals:

private slots:
    /**
     * @brief get_category_shot_test - тест вычисления категории события
     */
    void get_category_shot_test();

    /**
     * @brief get_bracket_index_list_test - тест вычисления номурованого списка индексов скобок
     */
    void get_bracket_index_list_test();

    /**
     * @brief get_regex_parsed_header_test - тест парсинга сезона из заголовка матча
     */
    void get_regex_parsed_header_test();

    /**
     * @brief create_event - создать событие с конкретными параметрами
     * @param blocked - заблокированный
     * @param bigchance - bigchance
     * @param footed - удар ногой
     * @param inbox - в пределах зоны
     * @return событие
     */
    Match::Event create_event(bool blocked, bool bigchance, bool footed, bool inbox);

private:
    // тестируемый матч
    Match m_match;

    /**
     * @brief The EventBuilder class сборщик события
     */
    class EventBuilder {
    private:
        bool m_blocked = false;
        bool m_bigchance = false;
        bool m_footed = false;
        bool m_inbox = false;

    public:
        Match::Event build() {
            Match::Event event(m_blocked, m_bigchance, m_footed, m_inbox);
            Match::get_shot_category(event);
            return event;
        }

        EventBuilder* blocked(bool blocked) { m_blocked = blocked; return this; }
        EventBuilder* bigchance(bool bigchance) { m_bigchance = bigchance; return this; }
        EventBuilder* footed(bool footed) { m_footed = footed; return this; }
        EventBuilder* inbox(bool inbox) { m_inbox = inbox; return this; }
    };
};

#endif // MATCHTEST_H
