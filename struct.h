#ifndef STRUCT_H
#define STRUCT_H
#include <QtCore>

/**
 * @brief The MatchInfo struct - информация о матче
 */
struct MatchInfo
{
    QString m_urlLive;
    QString m_urlShow;
    QString m_urlHomeData;
    QString m_ligaName;
    QString m_nameFirstTeam;
    QString m_idFirstTeam;
    QString m_idForNextUrl;
    QString m_nameSecondTeam;
    QString m_idMatch;
    QString m_year;
    QString m_year2;
    QString m_date;
    QString m_countryName;
    QString m_resultMatchFullTime;
    QString m_elap;
    QString m_date2;
    QList<MatchInfo> m_allShows;
    QStringList m_listElap;
};

#endif // STRUCT_H
