#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include "struct.h"
#include "htmldecoder.h"

#include <memory>
#include <condition_variable>

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QNetworkCookieJar>
#include <QNetworkCookie>
#include <QEventLoop>
#include <QUrl>
#include <QFile>


const QString GET_MODEL = "Model-last-Mode': '";
const QString GET_STAGE = "stageId: ";
const QString GET_MATCHS = "var matchDate = '";

class NetworkManager : public QObject
{
    Q_OBJECT
public:
    explicit NetworkManager(QObject *parent = nullptr);

signals:

public slots:
    void get_all_match(const QString & url);
    QString get_match(const QString & url, const QString & homeaway_url = QString(), bool fast_return = false);
    void parsing_match_show_page(MatchInfo & info);
    void parsing_match_show_page_2(MatchInfo & info, MatchInfo & buffInfo);
    void get_show_info(MatchInfo & info, MatchInfo & buffInfo);
    void get_show_info(MatchInfo & info);
    void parser_match(QString &);
    void parser_match(QString & buff,MatchInfo &info);

    void parse_year_from_url(int & year1, int & year2, const QString & url);

private slots:
    void wait(const ulong & sec);
    bool readLeageElap(QString & Leage, const QString & elap);

public:
    QList<MatchInfo> m_allMatchs;
    const QRegularExpression m_regex;

private:
    std::unique_ptr<QNetworkAccessManager> m_manager;
    QNetworkRequest m_request;
    QNetworkReply * m_reply;
    QEventLoop m_loop;
    QList<QNetworkCookie> m_cookies;
    QNetworkCookieJar * m_jar = new QNetworkCookieJar;
    QString m_paramMod, m_paramMod2, m_lastUrl;
    std::condition_variable m_cond_var;
    bool flagShow = false;
    QString m_current_home_away_url;
};

#endif // NETWORKMANAGER_H
