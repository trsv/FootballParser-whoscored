#include "io.h"

#include <QFile>
#include <QDebug>

IO::IO(QObject *parent) : QObject(parent)
{

}

void IO::save_to_json(const Match & match, const QString &path)
{
    // создаем файл по пути
    QFile file(path);

    // если файл невозможно открыть для записи
    if (!file.open(QIODevice::WriteOnly)) {
        // выводим ошибку и ничего не делаем
        qWarning() << "[IO] Error: cannot save to file:" << path;
        return;
    }

    // конвертируем матч в json
    auto json = QJsonDocument(convert_to_json(match));

    // записываем json в файл
    file.write(json.toJson());

    // закрываем файл
    file.close();
}

QJsonObject IO::convert_to_json(const Match & match, bool is_submatch)
{
    // данные матча
    auto data = match.get_data();

    // json-объект матча
    QJsonObject match_json;

    // данные для любого матча
    match_json.insert("url", match.m_url);
    match_json.insert("home", prepare_team(data.team1, is_submatch));
    match_json.insert("away", prepare_team(data.team2, is_submatch));
    match_json.insert("season", data.season);

    // если текущий объект это хед_ту_хед матч
    if (is_submatch) {
        // записывем played
        match_json.insert("played", data.played);
    } else {
        // иначе записываем остальные данные
        match_json.insert("championship", data.name_championship);
        match_json.insert("date", data.date_match);
        match_json.insert("time", data.time_match);
        match_json.insert("weather", data.weather);
    }

    // массив json-объектов для хед_ту_хед матчей
    QJsonArray sub_match_array;
    for (auto sub_match : match.m_submatch_list) {
        QJsonObject sub_match_json = convert_to_json(sub_match, true);
        sub_match_array.append(sub_match_json);
    }

    // если массив хед-матчей не пуст, до добавляем его в json-объект всего матча
    if (!sub_match_array.isEmpty()) match_json.insert("head_to_head", sub_match_array);

    // возвращаем сформированый json
    return match_json;
}

QJsonObject IO::prepare_team_goal(Match::Team * team)
{
    // подготавливаемый json-объект
    QJsonObject json;

    // заполняем список - номер гола и минута
    for (int i = 0; i < team->goal_minute_list.size(); i++) {
        QString key = QString::number(i + 1);
        json.insert(key, team->goal_minute_list[i]);
    }

    return json;
}

QJsonObject IO::prepare_team_card(Match::Team *team)
{
    QJsonObject json;

    // минута первой красной карточки
    json.insert("1", team->first_red_minute);

    // минута второй красной карточки
    json.insert("2", team->second_red_minute);

    return json;
}

QJsonObject IO::prepare_team_three_minute(Match::ThreeMinutes & minutes)
{
    QJsonObject json;

    // запись 3х минутных данных в json
    json.insert("75", minutes.before_75);
    json.insert("42", minutes.before_42);
    json.insert("21", minutes.before_21);

    return json;
}

QJsonObject IO::prepare_team(Match::Team *team, bool only_goals)
{
    QJsonObject json;

    // если нужно записать не только данные по голам
    if (!only_goals) {
        json.insert("name", team->name);
        json.insert("aligment", team->aligment);

        json.insert("red_card_minute_array", prepare_team_card(team));

        json.insert("pass_count", prepare_team_three_minute(team->pass));

        json.insert("noblocked_or_bigchance_and_blocked_count", prepare_team_three_minute(team->noblocked_or_bigchance_and_blocked));
        json.insert("bigchance_count", prepare_team_three_minute(team->bigchance));
        json.insert("noblocked_or_bigchance_and_blocked_left_or_right_from_penalty_count", prepare_team_three_minute(team->noblocked_or_bigchance_and_blocked_left_or_right_from_penalty));
    }

    json.insert("goal_minute_array", prepare_team_goal(team));
    json.insert("score", team->score);

    return json;
}
