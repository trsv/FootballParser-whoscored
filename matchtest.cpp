#include "matchtest.h"

MatchTest::MatchTest(QObject *parent) : QObject(parent) { }

void MatchTest::get_category_shot_test()
{
    create_event(false, false, true, true);

    create_event(false, false, true, false);
//    create_event(false, false, false, true);

    create_event(false, false, true, true);
//    create_event(false, false, false, true);

    // тест первой категории
//    QVERIFY(EventBuilder()
//            .bigchance(true)
//            ->blocked(true)
//            ->footed(true)
//            ->inbox(true)
//            ->build().category_list.contains(Match::Event::ShotCategory::First));

//    QVERIFY(EventBuilder()
//            .bigchance(true)
//            ->blocked(true)
//            ->footed(true)
//            ->inbox(true)
    //            ->build().category_list.contains(Match::Event::ShotCategory::First));
}

void MatchTest::get_bracket_index_list_test()
{
    QString raw = "[[][[]]]";
    Match::BracketIndexList true_index_list;
    true_index_list.insert(1, QPair<int, int>(0, 7));
    true_index_list.insert(2, QPair<int, int>(1, 2));
    true_index_list.insert(3, QPair<int, int>(3, 6));
    true_index_list.insert(4, QPair<int, int>(4, 5));

    auto index_list = Match::get_bracket_index_list(raw);

    QCOMPARE(true_index_list, index_list);
}

// тест парсинга заголовка - название чемпионата и сезон
void MatchTest::get_regex_parsed_header_test()
{
    // список тестируемых заголовков: сам заголовок и правильный сезон из него
    QMap <QString, QString> header_list;
    header_list.insert("Major League Soccer - 2016 - Major League Soccer", "2016");
    header_list.insert("Major League Soccer - 2016/2017 - Major League Soccer", "2016/2017");

    // читаем список
    for (auto header_list_iterator = header_list.begin(); header_list_iterator != header_list.end(); ++header_list_iterator) {

        // читаем заголовок и валидный сезон
        const QString header = header_list_iterator.key();
        const QString year_valid = header_list_iterator.value();

        // парсим заголовок
        const QRegularExpressionMatch test_match = m_match.get_regex_parsed_header(header);
        const QString year_test = test_match.captured(2).trimmed();

        // сравниваем года
        QCOMPARE(year_test, year_valid);
    }

}

Match::Event MatchTest::create_event(bool blocked, bool bigchance, bool footed, bool inbox)
{
    Match::Event event = EventBuilder().bigchance(bigchance)->blocked(blocked)->footed(footed)->inbox(inbox)->build();
    qDebug() << "category:" << event.category_list;

    return event;
}

