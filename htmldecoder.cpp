#include "htmldecoder.h"

QRegularExpression HtmlDecoder::m_regex("&#.*?;");
QRegularExpression HtmlDecoder::m_regex_for_home_away_url("\\d{4}-(.*?)$");

QTextDocument m_decoder;

HtmlDecoder::HtmlDecoder() { }

void HtmlDecoder::decode(QString & html)
{
    auto match = m_regex.globalMatch(html);

    QStringList encoded_symbols_list;

    while (match.hasNext()) {
        encoded_symbols_list << match.next().captured(0);
    }

    if (encoded_symbols_list.isEmpty()) {
        return;
    }

    encoded_symbols_list.removeDuplicates();

    for (auto encoded_symbol : encoded_symbols_list) {
        m_decoder.setHtml(encoded_symbol);
        auto decoded_symbol = m_decoder.toPlainText();

        html.replace(encoded_symbol, decoded_symbol);
    }
}

QString HtmlDecoder::parse_canonical(const QString &html)
{
    QString canon_start_key = "canonical\" href=\"";

    if (!html.contains(canon_start_key, Qt::CaseInsensitive)) {
        return QString();
    }

    QString canon_end_key = "\">";

    int canon_start_index = html.indexOf(canon_start_key, Qt::CaseInsensitive) + canon_start_key.length();
    int canon_end_index = html.indexOf(canon_end_key, canon_start_index, Qt::CaseInsensitive);
    int canon_length = canon_end_index - canon_start_index;

    QString url_canon = html.mid(canon_start_index, canon_length);

    return url_canon;
}

QString HtmlDecoder::parse_homeaway_url(const QString &canonical)
{
    return m_regex_for_home_away_url.match(canonical).captured(1);
}
