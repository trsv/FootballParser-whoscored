#include "match.h"
#include "matchtest.h"
#include "headlisttest.h"
#include "networkmanagertest.h"
#include "networkmanager.h"
#include "io.h"
#include "headlist.h"
#include "logger.h"

#include <QMultiMap>

#include <QCoreApplication>
#include <QtTest/QTest>

/**
 * @brief parse_match - спарсить матч по ссылке
 * @param url - ссылка на матч
 * @param error - флаг ошибки
 * @param network - менеджер сети
 * @return спарсенный матч
 */
Match parse_match(const QString & url, bool & error, NetworkManager * network, const QString & homeaway_url = QString()) {

    // матч
    Match match;

    // инициализируем ссылку
    match.m_url = url;

    // удаляем спецсимволы из ссылки
    match.m_url.remove("\r").remove("\n");

    // скачиваем страницу матча
    qDebug() << "\n[INFO] Parse" << match.m_url;
    if (!homeaway_url.isEmpty()) qDebug() << "[Main] HomeAway url" << homeaway_url;
    QString data_raw = network->get_match(match.m_url, homeaway_url, true);

    // ошибка, если страница пустая
    error = data_raw.isEmpty();

    // если ошибки нету
    if (!error) {
        // парсим матч
        bool success_parse = match.load(data_raw);

        // ошибка = провал парсинга
        error = !success_parse;
        qDebug() << "[Match] Parse match result:" << success_parse;
    }

    // логгируем ошибку
    if (error) {
        QString error_message = "[ERROR] Error while try get match:" + match.m_url;
        qWarning() << error_message;
        Logger::log_to_file(error_message, "log_error.txt");
    }

    // возвращаем матч
    return match;
}


/**
 * @brief parse_submatch_list - спарсить список хед_ту_хед матчей
 * @param parrent_match - главный матч
 * @param parrent_match_info - информация по главному матчу
 * @param network - менеджер сети
 */
void parse_submatch_list(Match & parrent_match, MatchInfo & parrent_match_info, NetworkManager * network) {

    qDebug() << "[Main] Parse submatch list...";

    // парсинг хед ту хед данных
    network->parsing_match_show_page(parrent_match_info);

    qDebug() << "[Main] Submatch list parsed. Creating valid list...";

    // список мачтей хед_ту_хед
    HeadList submatch_list;

    // сезон главного матча и вычисление первого сезона для хед_ту_хед
    submatch_list.set_current_season(parrent_match.get_data().season);

    QRegularExpression regex("^\\d+");
    QMultiMap <int, MatchInfo> sorted_matchinfo_list;

    // отсеиваем хед_ту_хед матчи по годам
    for (auto sub_match_raw : parrent_match_info.m_allShows) {

        int year = regex.match(sub_match_raw.m_year).captured(0).toInt();
        sorted_matchinfo_list.insert(year, sub_match_raw);

    }

    // сортируем список годов
    auto key_list = sorted_matchinfo_list.keys();
    std::reverse(key_list.begin(), key_list.end());

    // проходимся по списку матчей с помощью списка годов
    for (auto key : key_list) {

        // добавляем в список матчи
        for (auto value : sorted_matchinfo_list.values(key)) {

            if (!submatch_list.add(value)) break;

        }

    }

    // заполняем пустыми матчами
    submatch_list.fill_empty();

    auto submatch_map = submatch_list.get_list();

    // парсим хед_ту_хед матчи
    for (auto it = submatch_map.begin(); it != submatch_map.end(); ++it) {

//        qDebug() << "[INFO] Parse" << count << "match submatch:" << sub_match_raw.m_urlLive;

        // информация о хед-матче
        auto submatch_raw = it.value();

        // год хед-матча
        auto submatch_year = it.key();

        // пустой ли матч
        bool is_empty_match = submatch_raw.m_urlLive.isEmpty();

        // если матч пустой
        if (is_empty_match) {

            // тогда добавляем в список главного матча как пустой матч
            Match empty_match;

            // инициализируем сезон матча
            empty_match.set_empty(submatch_year, parrent_match.m_helper_data.is_single_season);

            // в список
            parrent_match.append_submatch(empty_match);

        } else {

            // иначе парсим матч
            bool error = false;
            Match sub_match = parse_match(submatch_raw.m_urlLive, error, network, parrent_match.m_helper_data.homeaway_url);

            // если нет ошибок парсинга и матч не плейоф, то в список
            if (!error && !sub_match.get_data().is_playoff) parrent_match.append_submatch(sub_match);
        }

    }

    qDebug() << "[Main] Submatch valid list created";

}

/**
 * @brief parse_league - парсим лигу по ссылке
 * @param io - менеджер ввода/вывода
 * @param network - менеджер сети
 * @param url - ссылка на лигу
 */
void parse_league(IO * io, NetworkManager * network, const QString & url) {

    // счетчик матча
    int count = 0;

    // парсим всю инфу о матчах
    network->get_all_match(url);

    // подсчет кол-ва ссылок на матчи
    QFile file("league_match_url_list.txt");
    file.open(QIODevice::WriteOnly);
    for (auto match_info : network->m_allMatchs) {
        file.write((match_info.m_urlLive + "\r\n").toStdString().c_str());
    }
    file.close();

    // путь к папке лиги с матчами
    QString path;
    if (network->m_allMatchs.isEmpty()) {
        qWarning() << "[Main] List allMatch is empty. Can`t create league folder";
    } else {
        path = "out/" + network->m_allMatchs[0].m_ligaName + network->m_allMatchs[0].m_year.replace("/", "-");
    }

    // создаем папку
    QDir dir;
    if (!dir.exists(path)) {
        dir.mkdir(path);
        qDebug() << "[Main] League folder:" << path;
    }

    // парсим матчи
    for (auto match_raw : network->m_allMatchs) {

        qDebug() << "\n===========================";
        qDebug() << "[Main] Try parse main full match" << match_raw.m_urlLive;

        // парсим конкретный матч
        bool error = false;
        Match match = parse_match(match_raw.m_urlLive, error, network);

        // если не спарсили, то логгируем ошибку
        if (error) {
            QString error_message = "[ERROR] Failed parse main full match. Skip:" + match.m_url;
            qWarning() << error_message;
            Logger::log_to_file(error_message, "log_error.txt");
        } else {
            // иначе парсим список хед-матчей
            parse_submatch_list(match, match_raw, network);

            // и записываем весь главный матч в файл
            io->save_to_json(match, QString(path + "/%1.json").arg(count));
        }

        // увеличиваем счетчик матчей
        ++count;
    }

}

/**
 * @brief parse_match_prod - продакшн парсинг (со всеми данными) матча по ссылке
 * @param io - менеджер ввода/вывода
 * @param network - менеджер сети
 * @param url - ссылка на матч
 */
void parse_match_prod(IO * io, NetworkManager * network, const QString & url) {

    // создаем папку для тестов
    QDir().mkdir("test");

    // ссылка для хед-матчей
    QString show_url = url;
    show_url.replace("/Live/", "/Show/");

    // парсим матч
    bool error = false;
    Match match = parse_match(url, error, network);

    // логгируем ошибку или записываем в файл
    if (error) {
        QString error_message = "[ERROR] Failed parse main full match. Skip:" + match.m_url;
        qWarning() << error_message;
        Logger::log_to_file(error_message, "log_error.txt");
    } else {

        // информация о матче
        MatchInfo info;
        info.m_urlShow = match.m_helper_data.show_url;
        info.m_urlLive = url;
        info.m_urlHomeData = match.m_helper_data.home_vs_away_url;

        parse_submatch_list(match, info, network);
        io->save_to_json(match, "test/test_match_prod.json");
    }
}

/**
 * @brief from_file - спарсить список лиг из файла
 * @param path - путь к файлу со списком ссылок на лиги
 * @param network - менеджер сети
 * @param io менеджер ввода/вывода
 */
void from_file(const QString & path, NetworkManager * network, IO * io) {
    // файл со ссылками
    QFile file(path);

    // читаем список
    file.open(QIODevice::ReadOnly);

    QStringList url_list;
    while (!file.atEnd()) {
        QString url = file.readLine();
        url = url.trimmed();

        if (!url.isEmpty()) url_list << url;
    }

    file.close();

    // парсим лиги по ссылкам
    for (auto url : url_list) {
        parse_league(io, network, url);
    }

}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // запуск тестов
    QTest::qExec(new MatchTest, argc, argv);
    QTest::qExec(new HeadListTest, argc, argv);
    QTest::qExec(new NetworkManagerTest, argc, argv);

    // менеджер ввода/вывода
    IO io;

    // менеджер сети
    NetworkManager network;

    // тестовый парсинг лиги
//    parse_league(&io, &network, "https://www.whoscored.com/Regions/252/Tournaments/2/England-Premier-League");

    // тестовый парсинг матча с хедами
    parse_match_prod(&io, &network, "https://www.whoscored.com/Matches/1173132/Live/China-Super-league-2017-");

//     тестовый парсинг матча без хедов
//    bool error = false;
//    Match match = parse_match("https://www.whoscored.com/Matches/1103005/Live/Spain-La-Liga-2016-2017-Osasuna-Eibar", error, &network);
//    if (error) {
//        qWarning() << "test parse failed";
//    } else {
//        io.save_to_json(match, "t.json");
//    }

    // парсинг лиг из файла
//    from_file("url_list.txt", &network, &io);

    qDebug() << "App end";
    return a.exec();
}
