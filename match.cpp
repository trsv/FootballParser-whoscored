#include "match.h"
#include "htmldecoder.h"

#include <QFile>
#include <QDebug>
#include <QRegularExpression>
#include <QDateTime>
#include <QStringList>
#include <QStack>
#include <QTextDecoder>

Match::Match() {}

bool Match::load(const QString &path, bool from_file)
{
    // если нужно парсить из файла
    if (from_file) {

        // то читаем файл
        QFile file(path);

        // если файл не открыть
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            // то возвращаем провал парсинга
            qWarning() << "[IO] Error open file:" << path;
            return false;
        }

        // иначе инициализируем исходные данные матча
        m_data_raw = file.readAll();
        file.close();
    } else {

        // если парсим не из файла, то сюда уже передали исходные данные
        // тогда просто инициализируем
        m_data_raw = path;
    }

    HtmlDecoder::decode(m_data_raw);

    // исходные данные матча
    QString data_raw;

    // ключ поиска json-данных
    QString var_match = "var matchCentreData =";

    // ключ поиска начальных данных в массиве InitialData
    QString var_initial = "var initialMatchDataForScrappers =";

    // если есть данные в формате json
    if (m_data_raw.contains(var_match, Qt::CaseInsensitive)) {

        // то вырезаем эти данные из исходных
        int start_index = m_data_raw.indexOf(var_match, Qt::CaseInsensitive) + var_match.length();
        int end_index = m_data_raw.indexOf(";", start_index);
        int length = end_index - start_index;

        data_raw = m_data_raw.mid(start_index, length);

        // парсим в json
        QJsonParseError json_error;
        auto doc = QJsonDocument::fromJson(data_raw.toUtf8(), &json_error);

        // в случае ошибки парсинга логгируем и прекращаем работу
        if (doc.isEmpty() || doc.isNull()) {
            qWarning() << "[Match] Failed parse json:" << data_raw;
            return false;
        }

        // снова проверяем на ошибки
        bool is_success = json_error.error == QJsonParseError::NoError;

        // если ОК, то парсим json
        if (is_success) {
            m_doc = doc.object();
            parse();

            return true;
        } else {
            qWarning() << "[Match] Failed parse json:" << data_raw;
            return false;
        }

        // иначе, если на странице есть массив InitialData
    } else if (m_data_raw.contains(var_initial, Qt::CaseInsensitive)) {

        // вырезаем этот массив
        int start_index = m_data_raw.indexOf(var_initial, Qt::CaseInsensitive) + var_initial.length();
        int end_index = m_data_raw.indexOf(";", start_index);
        int length = end_index - start_index;

        data_raw = m_data_raw.mid(start_index, length).trimmed();

        // парсим из него сезон и чемпионат
        parse_season_and_championship();

        // парсим остальные данные
        parse_initial_array(data_raw);

        return true;

    } else {
        // если никаких данных нет
        qWarning() << "[Match] Failed parse:" << data_raw;
        return false;
    }
}

void Match::parse()
{
    // парсим список expanded-минут
    parse_expanded_minute_list();
    // парсим лимит таймов
    parse_period_minute_limit();

    // парсим команду 1
    parse_team(m_doc["home"].toObject());

    // парсим команду 2
    parse_team(m_doc["away"].toObject());

    // временный список, чтоб отсортировать события по id
    QMultiMap <int, QJsonObject> t_list;

    for (auto event_raw : m_doc["events"].toArray()) {
        auto object = event_raw.toObject();
        int id = object["eventId"].toInt();

        t_list.insert(id, object);
    }

    // парсим список событий по командам
    for (auto event_raw : t_list) {
        Event event = parse_event(event_raw);
        int team_number = m_team_dictionary[event.team_id];

        m_team_list[team_number].event_list.insert(event.id, event);
    }

    // указатель на 1ю команду
    m_data.team1 = &m_team_list[1];

    // указатель на 2ю команду
    m_data.team2 = &m_team_list[2];

    // парсим счет
    m_data.team1->score = QRegularExpression("^\\d+").match(m_doc["score"].toString()).captured(0).toInt();
    m_data.team2->score = QRegularExpression("\\d+$").match(m_doc["score"].toString()).captured(0).toInt();

    // парсим погоду
    m_data.weather = m_doc["weatherCode"].toString().toInt();

    // парсим сезон и чемпионат
    parse_season_and_championship();

    // парсим время и дату
    parse_datetime();

    // обрабатываем список событий для 1й команды
    filter_event_list(1);

    // обрабатываем список событий для 2й команды
    filter_event_list(2);

    // постобработка данных
    post_parse();
}

void Match::parse_initial_array(const QString & raw)
{
    auto bracket_list = get_bracket_index_list(raw);

    int index_start = bracket_list[5].first + 1;
    int index_end = bracket_list[5].second;

    QString array_raw = raw.mid(index_start, index_end - index_start);
    auto node_list = get_node_list(array_raw);

    QRegularExpression regex_minute("^\\d+");
    QRegularExpression regex_second_team("\\d$");

    Team team1;
    Team team2;

    m_team_list.insert(1, team1);
    m_team_list.insert(2, team2);

    m_data.team1 = &m_team_list[1];
    m_data.team2 = &m_team_list[2];

    for (auto node : node_list) {
        for (auto inner_node_raw : get_node_list(node)) {

            if (inner_node_raw.contains("goal", Qt::CaseInsensitive)) {

                int minute = regex_minute.match(node).captured(0).toInt();

                if (minute > 75) continue;

                bool is_first_team = regex_second_team.match(node).captured(0) != "1";

                Team * team = is_first_team ? m_data.team1 : m_data.team2;

                if (team->goal_minute_list.size() < 3) {
                    team->goal_minute_list.append(minute);
                } else {
                    break;
                }

            }

        }
    }

    // парсинг счета
    int score_index_start = bracket_list[3].first + 1;
    int score_index_end = bracket_list[3].second;

    QString score_raw = raw.mid(score_index_start, score_index_end - score_index_start);
    parse_score_from_initial_array(score_raw);

    post_parse();
}

void Match::parse_score_from_initial_array(const QString &raw)
{
    auto match = QRegularExpressionMatch();
    raw.lastIndexOf(QRegularExpression ("'\\d+ : \\d+'"), -1, &match);
    QString score_raw = match.captured(0).remove("'").trimmed();

    auto score_array = score_raw.split(':', QString::SkipEmptyParts);
    int team1_score = score_array[0].toInt();
    int team2_score = score_array[1].toInt();

    m_data.team1->score = team1_score;
    m_data.team2->score = team2_score;

}

void Match::parse_expanded_minute_list()
{
    auto expanded_raw = m_doc["expandedMinutes"].toObject();
    auto first_half = expanded_raw["1"].toObject();
    auto second_half = expanded_raw["2"].toObject();

    m_expanded_minute_list.insert(1, QMap <int, int>());
    m_expanded_minute_list.insert(2, QMap <int, int>());

    for (auto it = first_half.begin(); it != first_half.end(); ++it) {
        int expanded_minute = it.value().toInt();
        int minute = it.key().toInt();

        m_expanded_minute_list[1].insert(minute, expanded_minute);
    }

    for (auto it = second_half.begin(); it != second_half.end(); ++it) {
        int expanded_minute = it.value().toInt();
        int minute = it.key().toInt();

        m_expanded_minute_list[2].insert(minute, expanded_minute);
    }
}

void Match::parse_period_minute_limit()
{
    auto limits = m_doc["periodMinuteLimits"].toObject();

    m_period_minute_limit.first_period = limits["1"].toInt();
    m_period_minute_limit.second_period = limits["2"].toInt();
}

Match::Event Match::parse_event(const QJsonObject & json)
{
    Event event;

    event.json = json;
    event.id = json["eventId"].toInt();
    event.team_id = json["teamId"].toInt();

    event.minute = json["minute"].toInt();
    event.minute_expanded = json["expandedMinute"].toInt();
    event.period = json["period"].toObject()["value"].toInt();

    for (auto id_raw : json["satisfiedEventsTypes"].toArray()) {
        int value = id_raw.toInt();
        event.type_id_list.insert(value);
    }

    bool is_shot = json["isShot"].toBool();
    bool is_goal = json["isGoal"].toBool();

    if (is_shot && !is_goal) {
        event.type = Event::Shot;
    } else if (is_goal) {
        event.type = Event::Goal;
    } else if (event.type_id_list.contains(67)) {
        event.type = Event::Card;
    }

    event.is_bigchance = event.type_id_list.contains(200) || event.type_id_list.contains(201);
    event.is_blocked = event.type_id_list.contains(10);
    event.is_not_out_of_box = !event.type_id_list.contains(2);
    event.is_footed = event.type_id_list.contains(11) || event.type_id_list.contains(12);
    event.is_owngoal = event.type_id_list.contains(22);

    get_shot_category(event);

    return event;
}

Match::Team Match::parse_team(const QJsonObject &json)
{
    Team team;

    auto pass_array = json["stats"].toObject()["passesTotal"].toObject();

    QMap <int, int> map_t;
    for (auto it = pass_array.begin(); it != pass_array.end(); ++it) {

        int minute = it.key().toInt();
        int pass = QString::number(it.value().toDouble()).toInt();

        map_t.insert(minute, pass);
    }

    int summ = 0;
    for (auto it = map_t.begin(); it != map_t.end(); ++it) {
        summ += it.value();
        team.pass_list.insert(it.key(), summ);
    }

    team.json = json;
    team.id = json["teamId"].toInt();
    team.name = json["name"].toString();

    parse_team_pass(team);

    QString aligment_raw = json["formations"].toArray()[0].toObject()["formationName"].toString();
    QStringList aligment_list;

    QRegularExpression regex("\\d");
    QRegularExpressionMatchIterator i = regex.globalMatch(aligment_raw);
    while (i.hasNext()) aligment_list << i.next().captured();

    team.aligment = aligment_list.join("-");

    m_team_dictionary.insert(team.id, m_team_dictionary.size() + 1);
    m_team_list.insert(m_team_dictionary.size(), team);

    return team;
}

void Match::parse_team_pass(Match::Team & team)
{
    auto keys = team.pass_list.keys();

    int key_21 = expanded_minute(20, 1, keys);
    int key_42 = expanded_minute(41, 1, keys);
    int key_75 = expanded_minute(74, 2, keys);

    team.pass.before_21 = team.pass_list[key_21];
    team.pass.before_42 = team.pass_list[key_42];
    team.pass.before_75 = team.pass_list[key_75];
}

void Match::filter_event_list(int team_number)
{
    Team * team = &m_team_list[team_number];

    for (auto event : team->event_list) {

        if (event.type == Event::EventType::Goal) event.minute++;

        if (event.minute > 75) continue;

        filter_event(team, event);
    }
}

void Match::filter_event(Match::Team * team, Match::Event & event)
{
    switch (event.type) {
    case Event::Card:

        if (team->first_red_minute == -1) team->first_red_minute = event.minute;
        else if (team->second_red_minute == -1) team->second_red_minute = event.minute;

        break;

    case Event::Goal:

        if (event.period == 1 && (event.minute - 1 >= m_period_minute_limit.first_period)) {
            event.minute = 45;
        }

        if (event.is_owngoal) {

            int current_team_number = m_team_dictionary.value(team->id);
            current_team_number == 1 ? current_team_number = 2 : current_team_number = 1;

            Team * another_team = &m_team_list[current_team_number];

            // добавляем минуту в массив, если всего меньше 3х
            if (another_team->goal_minute_list.size() < 3) {
                another_team->goal_minute_list.append(event.minute);
            }

        } else {

            // добавляем минуту в массив, если всего меньше 3х
            if (team->goal_minute_list.size() < 3) {
                team->goal_minute_list.append(event.minute);
            }

        }

        if (event.is_owngoal) return;

        if (event.category_list.contains(Event::ShotCategory::First)) {
            filter_shot(team->noblocked_or_bigchance_and_blocked, event.minute);
        }

        if (event.category_list.contains(Event::ShotCategory::Second)) {
            if (!m_set.contains(event.minute_expanded)) {
                filter_shot(team->bigchance, event.minute);
                m_set.insert(event.minute_expanded);
            }
        }

        if (event.category_list.contains(Event::ShotCategory::Third)) {
            filter_shot(team->noblocked_or_bigchance_and_blocked_left_or_right_from_penalty, event.minute);
        }

        break;

    case Event::Shot:

        if (event.category_list.contains(Event::ShotCategory::First)) {
            filter_shot(team->noblocked_or_bigchance_and_blocked, event.minute);
        }

        if (event.category_list.contains(Event::ShotCategory::Second)) {
            if (!m_set.contains(event.minute_expanded)) {
                filter_shot(team->bigchance, event.minute);
                m_set.insert(event.minute_expanded);
            }
        }

        if (event.category_list.contains(Event::ShotCategory::Third)) {
            filter_shot(team->noblocked_or_bigchance_and_blocked_left_or_right_from_penalty, event.minute);
        }

        break;

    default:
        break;
    }
}

void Match::filter_shot(Match::ThreeMinutes & categorys, int minute)
{
    if (minute < 21) {
        categorys.before_21++;
        categorys.before_42++;
        categorys.before_75++;
    } else if (minute < 42) {
        categorys.before_42++;
        categorys.before_75++;
    } else if (minute < 75) {
        categorys.before_75++;
    } else return;
}

void Match::parse_season_and_championship()
{
    QString block_name = "breadcrumb-nav";
    int start_index = m_data_raw.indexOf(block_name, Qt::CaseInsensitive) + block_name.length();
    int end_index = m_data_raw.indexOf("</a>", start_index, Qt::CaseInsensitive);

    QString raw = m_data_raw.mid(start_index, end_index - start_index);
    raw = raw.mid(raw.lastIndexOf("\">") + 2).trimmed();

    QRegularExpressionMatch regex_result = get_regex_parsed_header(raw);

    QString name = regex_result.captured(1).trimmed();
    QString year = regex_result.captured(2).trimmed();

    m_data.name_championship = name;
    m_data.season = year;

    m_data.is_playoff = raw.contains("playoff", Qt::CaseInsensitive);
}

void Match::parse_datetime()
{
    QString datetime_raw = m_doc["startTime"].toString();

    auto match_datetime = QDateTime::fromString(datetime_raw, "yyyy-MM-ddThh:mm:ss");
    m_data.date_match = match_datetime.toString("dd.MM.yyyy");
    m_data.time_match = match_datetime.toString("hh:mm");
}

void Match::post_parse(bool is_empty)
{
    if (is_empty) {
        Team team;

        m_team_list.insert(1, team);

        m_data.team1 = &m_team_list[1];
        m_data.team2 = &m_team_list[1];

        set_to_zero_team(m_data.team1);
        set_to_zero_team(m_data.team2);

    } else {

        qSort(m_data.team1->goal_minute_list.begin(), m_data.team1->goal_minute_list.end());
        qSort(m_data.team2->goal_minute_list.begin(), m_data.team2->goal_minute_list.end());

        while (m_data.team1->goal_minute_list.size() < 3) {
            m_data.team1->goal_minute_list.append(0);
        }

        while (m_data.team2->goal_minute_list.size() < 3) {
            m_data.team2->goal_minute_list.append(0);
        }

        if (m_data.team1->first_red_minute == -1) m_data.team1->first_red_minute = 0;
        if (m_data.team1->second_red_minute == -1) m_data.team1->second_red_minute = 0;

        if (m_data.team2->first_red_minute == -1) m_data.team2->first_red_minute = 0;
        if (m_data.team2->second_red_minute == -1) m_data.team2->second_red_minute = 0;

        m_helper_data.show_url = m_url;
        m_helper_data.show_url.replace("/Live/", "/Show/");

        auto date_t = QDate::fromString(m_data.date_match, "dd.MM.yyyy");
        m_helper_data.home_vs_away_url = QString("https://www.whoscored.com/teamsfeed/%1/PreviousMeetings/?awayTeamId=%2&d=%3&type=1")
                .arg(m_data.team1->id)
                .arg(m_data.team2->id)
                .arg(date_t.toString("yyyyMMd"));

        auto canonical = HtmlDecoder::parse_canonical(m_data_raw);
        m_helper_data.homeaway_url = HtmlDecoder::parse_homeaway_url(canonical);
    }
}

int Match::expanded_minute(int minute, int period, QList <int> & key_list)
{
    int new_minute = m_expanded_minute_list[period][minute];

    while (!key_list.contains(new_minute)) {
        new_minute = m_expanded_minute_list[period][--minute];
        if(minute < 0)
            break;
    }

    return new_minute;
}

// обнуляем значения для значений до 21, 42 и 75 минут в матче
void Match::set_to_zero_three_minute(Match::ThreeMinutes &minutes)
{
    minutes.before_21 = 0;
    minutes.before_42 = 0;
    minutes.before_75 = 0;
}

// обнуляем значения для всей команды
void Match::set_to_zero_team(Team *team)
{
    set_to_zero_three_minute(team->bigchance);
    set_to_zero_three_minute(team->noblocked_or_bigchance_and_blocked);
    set_to_zero_three_minute(team->noblocked_or_bigchance_and_blocked_left_or_right_from_penalty);

    team->first_red_minute = 0;
    team->second_red_minute = 0;


    while (team->goal_minute_list.size() < 3) {
        team->goal_minute_list.append(0);
    }

}

void Match::get_shot_category(Match::Event &event)
{
    if (!event.is_blocked || (event.is_bigchance && event.is_blocked)) {
        event.category_list.insert(Event::ShotCategory::First);
    }

    if (event.is_bigchance) {
        event.category_list.insert(Event::ShotCategory::Second);
    }

    if ((!event.is_blocked || (event.is_bigchance && event.is_blocked)) && event.is_footed && event.is_not_out_of_box) {
        event.category_list.insert(Event::ShotCategory::Third);
    }
}

Match::BracketIndexList Match::get_bracket_index_list(const QString &raw)
{
    QStack <int> stack;
    BracketIndexList index_list;

    for (int char_index = 0, bracket_number = 0; char_index < raw.length(); char_index++) {

        QChar c = raw[char_index];

        if (c == '[') {

            stack.push(++bracket_number);
            index_list.insert(bracket_number, QPair<int, int>(char_index, -1));

        } else if (c == ']') {

            int left_bracket_index = stack.pop();
            index_list[left_bracket_index].second = char_index;

        }

    }

    return index_list;
}

QList<QString> Match::get_node_list(const QString &raw)
{
    QList <QString> node_list;
    auto bracket_list = get_bracket_index_list(raw);

    int node_start = -1, node_end = -1;
    for (auto bracket : bracket_list) {
        node_start = bracket.first + 1;
        if (node_end < bracket.second) {
            node_end = bracket.second;

            QString node_raw = raw.mid(node_start, node_end - node_start);
            node_list.append(node_raw);
        }
    }

    return node_list;
}

QRegularExpressionMatch Match::get_regex_parsed_header(const QString &header_raw)
{
    QRegularExpressionMatch regex_result = QRegularExpression("(.*?)-.*?(\\d+\\/\\d+)").match(header_raw);

    // если есть совпадение по первому паттерну с двумя годами, то возвращаем результат
    if (regex_result.hasMatch()) {

        // у текущего матча сезон в формате - год/год
        m_helper_data.is_single_season = false;
        return regex_result;

    } else {

        // сезон в формате - год
        m_helper_data.is_single_season = true;

        // иначе возвращаем результат по второму паттерну
        QRegularExpressionMatch regex_result2 = QRegularExpression("(.*?)-.*?(\\d+)").match(header_raw);
        return regex_result2;
    }
}

void Match::append_submatch(Match &match)
{
    match.parse_played();
    m_submatch_list.append(match);
}

// создать пустой хед-матч с переданным сезоном
void Match::set_empty(int year, bool is_single_season)
{
    post_parse(true);

    // если сезон - один год
    if (is_single_season) {
        m_data.season = QString("%1").arg(year);
    } else {
        m_data.season = QString("%1/%2").arg(year).arg(year + 1);
    }
}

void Match::parse_played()
{
    m_data.played = m_url.isEmpty() ? 0 : 1;
}
