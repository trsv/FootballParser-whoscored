#ifndef NETWORKMANAGERTEST_H
#define NETWORKMANAGERTEST_H

#include "networkmanager.h"
#include "htmldecoder.h"

#include <QObject>
#include <QtTest/QtTest>

/**
 * @brief The NetworkManagerTest class - набор тестов для менеджера сети
 */
class NetworkManagerTest : public QObject
{
    Q_OBJECT

public:
    explicit NetworkManagerTest(QObject * parent = nullptr);

private slots:
    /**
     * @brief parse_year_test - тест для парсинга годов из ссылки
     */
    void parse_year_test();

    void parse_home_year_away_test();

private:
    // тестируемый менеджер
    NetworkManager m_manager;
};

#endif // NETWORKMANAGERTEST_H
