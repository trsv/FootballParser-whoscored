QT += testlib gui network

CONFIG += c++11 console
CONFIG -= app_bundle
LIBS += -lz
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    match.cpp \
    matchtest.cpp \
    qcompressor.cpp \
    io.cpp \
    networkmanager.cpp \
    headlist.cpp \
    headlisttest.cpp \
    logger.cpp \
    networkmanagertest.cpp \
    htmldecoder.cpp

HEADERS += \
    match.h \
    matchtest.h \
    qcompressor.h \
    io.h \
    networkmanager.h \
    struct.h \
    headlist.h \
    headlisttest.h \
    logger.h \
    networkmanagertest.h \
    htmldecoder.h
