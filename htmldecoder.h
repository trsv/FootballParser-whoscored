#ifndef HTMLDECODER_H
#define HTMLDECODER_H

#include <QString>
#include <QRegularExpression>
#include <QTextDocument>

class HtmlDecoder
{
public:
    HtmlDecoder();

public:
    static void decode(QString & html);
    static QString parse_canonical(const QString & html);
    static QString parse_homeaway_url(const QString & canonical);

private:
    static QRegularExpression m_regex;
    static QRegularExpression m_regex_for_home_away_url;
};

#endif // HTMLDECODER_H
