#include "networkmanager.h"
#include "qcompressor.h"
#include <QDate>
#include <QLocale>

NetworkManager::NetworkManager(QObject *parent) :
    QObject(parent),
    m_regex("-(\\d{4})")
{
    m_manager.reset(new QNetworkAccessManager);
}

void NetworkManager::get_all_match(const QString & url)
{
    m_allMatchs.clear();

    QUrl url2(url);
    m_lastUrl = url;
    QNetworkRequest request;
    request.setRawHeader("Host", "www.whoscored.com");
    request.setRawHeader("Connection", "keep-alive");
    request.setRawHeader("Cache-Control", "max-age=0");
    request.setRawHeader("Upgrade-Insecure-Requests", "1");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    request.setRawHeader("Accept-Encoding", "gzip, deflate, br");
    if(!m_cookies.isEmpty())
        m_jar->setCookiesFromUrl(m_cookies, url2);
    request.setUrl(url2);
    m_reply = m_manager->get(request);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result3;

        try
        {
            if(m_reply->error() == QNetworkReply::NoError)
            {
                QByteArray strBuf = m_reply->readAll();
                QByteArray result2;

                QFile file("result2.txt");
                file.open(QIODevice::WriteOnly);
                if(QCompressor::gzipDecompress(strBuf, result2))
                {
                    qDebug() << "Decompressed text is: " ;
                }
                else
                    qDebug() << "Can't decompress";
                result3 = result2;
                file.write(result3.toStdString().c_str());
                file.close();
                m_cookies = qvariant_cast<QList<QNetworkCookie>>(m_reply->header(QNetworkRequest::SetCookieHeader));

                if(result3.indexOf("<head><META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, NOFOLLOW") != -1)
                {
                    get_all_match(url);
                }
                else if(result3.indexOf("<meta name=\"title\" content") != -1)
                {
                    qDebug() << "GO parsing";
                    QString stageId = result3.mid(result3.indexOf(GET_STAGE) + GET_STAGE.length(), result3.indexOf(",", result3.indexOf(GET_STAGE) + GET_STAGE.length()) - (result3.indexOf(GET_STAGE) + GET_STAGE.length()));

                    QString canonical_url = HtmlDecoder::parse_canonical(result3);

                    int year1 = 0;
                    int year2 = 0;

                    this->parse_year_from_url(year1, year2, canonical_url);

                    m_paramMod = result3.mid(result3.indexOf(GET_MODEL) + GET_MODEL.length(), result3.indexOf("'", result3.indexOf(GET_MODEL) + GET_MODEL.length()) - (result3.indexOf(GET_MODEL) + GET_MODEL.length()));

                    if(year2 == 0 || year1 == 0)
                    {
                        if(year1 == 0)
                            year1 = m_lastUrl.mid(m_lastUrl.lastIndexOf("-") + 1).toInt();
                        for(int i = 1; i <= 53; ++i)
                        {
                            QString url_torn;
                            QTextStream ts(&url_torn);
                            ts << "https://www.whoscored.com/tournamentsfeed/" << stageId << "/Fixtures/?d=" << year1 <<"W" << i <<"&isAggregate=false";

                            wait(1500);
                            get_match(url_torn);
                        }
                    }
                    else
                    {
                        for(int i = 1; i <= 53; ++i)
                        {
                            QString url_torn;
                            QTextStream ts(&url_torn);
                            if(i <= 26)
                            {
                                ts << "https://www.whoscored.com/tournamentsfeed/" << stageId << "/Fixtures/?d=" << year2 <<"W" << i <<"&isAggregate=false";
                            }
                            else
                                ts << "https://www.whoscored.com/tournamentsfeed/" << stageId << "/Fixtures/?d=" << year1 <<"W" << i <<"&isAggregate=false";
                            wait(1500);
                            get_match(url_torn);
                        }
                    }
                }
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::runtime_error(str.c_str());
            }
        }
        catch(const std::exception & except)
        {
            qDebug() << except.what();
    }
}

QString NetworkManager::get_match(const QString &url, const QString &homeaway_url, bool fast_return)
{
    QUrl url2(url);
    QNetworkRequest request;
    request.setRawHeader("Host", "www.whoscored.com");
    request.setRawHeader("Connection", "keep-alive");
    request.setRawHeader("Accept", "text/plain, */*; q=0.01");
    request.setRawHeader("Model-last-Mode", m_paramMod.toStdString().c_str());
    request.setRawHeader("X-Requested-With", "XMLHttpRequest");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");

    request.setRawHeader("Referer", m_lastUrl.toStdString().c_str());
    request.setRawHeader("Accept-Encoding", "gzip, deflate, br");
    if(!m_cookies.isEmpty())
        m_jar->setCookiesFromUrl(m_cookies, url2);
    request.setUrl(url2);

    QThread::msleep(1500);

    m_reply = m_manager->get(request);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result, result3;


    if (m_reply->error() != QNetworkReply::NoError) {
        qWarning() << "[NetworkManager]" << m_reply->errorString();
        return QString();
    }

    QByteArray strBuf = m_reply->readAll();
    QByteArray result2;

    if (!QCompressor::gzipDecompress(strBuf, result2)) {
        qWarning() << "[NetworkManager]" << "Can't decompress:" << url;
        return QString();
    }

    result3 = result2;
    result = result3;

    qDebug() << "[NetworkManager] get_match success:" << url;

    if (fast_return) {
        m_current_home_away_url = homeaway_url;
        return result;
    } else {

        if(result3 != "[]") {
            this->parser_match(result3);
        }

        return QString();
    }
}

void NetworkManager::wait(const ulong &sec)
{
    QThread::msleep(sec);

    /*std::mutex Synk_Req;
    std::unique_lock<std::mutex> lk(Synk_Req);
    auto timePoint = std::chrono::system_clock::now();
    m_cond_var.wait_until(lk, timePoint + std::chrono::milliseconds(sec))*/;
}

bool NetworkManager::readLeageElap(QString &Leage, const QString & elap)
{
    QFile file("ligas.txt");
    file.open(QIODevice::ReadOnly);
    QString result = file.readAll();
    result = result.mid(result.indexOf(Leage)+Leage.length(), result.indexOf("]", result.indexOf(Leage)) + 1 - result.indexOf(Leage) - Leage.length());
//    qDebug() << result;
    file.close();
    if(result.indexOf(elap) == -1)
        return false;
    return true;
}

void NetworkManager::parsing_match_show_page(MatchInfo & info)
{
    QUrl url2(info.m_urlShow);
    QNetworkRequest request;
    request.setRawHeader("Host", "www.whoscored.com");
    request.setRawHeader("Connection", "keep-alive");
    request.setRawHeader("Cache-Control", "max-age=0");
    request.setRawHeader("Upgrade-Insecure-Requests", "1");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    request.setRawHeader("Referer", info.m_urlLive.toStdString().c_str());
    request.setRawHeader("Accept-Encoding", "gzip, deflate, br");
    if(!m_cookies.isEmpty())
        m_jar->setCookiesFromUrl(m_cookies, url2);
    request.setUrl(url2);
    wait(3000);
    m_reply = m_manager->get(request);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result3;

        try
        {
            if(m_reply->error() == QNetworkReply::NoError)
            {
                QByteArray strBuf = m_reply->readAll();
                QByteArray result2;
                if(QCompressor::gzipDecompress(strBuf, result2))
                {
                    qDebug() << "Decompressed text is: " ;
                }
                else
                    qDebug() << "Can't decompress";
                result3 = result2;
                m_paramMod2 = result3.mid(result3.indexOf(GET_MODEL) + GET_MODEL.length(), result3.indexOf("'", result3.indexOf(GET_MODEL) + GET_MODEL.length()) - (result3.indexOf(GET_MODEL) + GET_MODEL.length()));
                get_show_info(info);
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::runtime_error(str.c_str());
            }
        }
        catch(const std::exception & except)
        {
            qDebug() << except.what();
    }
}

void NetworkManager::parsing_match_show_page_2(MatchInfo &info, MatchInfo &buffInfo)
{
    QUrl url2(buffInfo.m_urlShow);
    QNetworkRequest request;
    request.setRawHeader("Host", "www.whoscored.com");
    request.setRawHeader("Connection", "keep-alive");
    request.setRawHeader("Cache-Control", "max-age=0");
    request.setRawHeader("Upgrade-Insecure-Requests", "1");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    request.setRawHeader("Referer", buffInfo.m_urlLive.toStdString().c_str());
    request.setRawHeader("Accept-Encoding", "gzip, deflate, br");
    if(!m_cookies.isEmpty())
        m_jar->setCookiesFromUrl(m_cookies, url2);
    request.setUrl(url2);
    wait(3000);
    m_reply = m_manager->get(request);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result3;

        try
        {
            if(m_reply->error() == QNetworkReply::NoError)
            {
                QByteArray strBuf = m_reply->readAll();
                QByteArray result2;
                if(QCompressor::gzipDecompress(strBuf, result2))
                {
                    qDebug() << "Decompressed text is: " ;
                }
                else
                    qDebug() << "Can't decompress";
                result3 = result2;
                m_paramMod2 = result3.mid(result3.indexOf(GET_MODEL) + GET_MODEL.length(), result3.indexOf("'", result3.indexOf(GET_MODEL) + GET_MODEL.length()) - (result3.indexOf(GET_MODEL) + GET_MODEL.length()));
                get_show_info(info, buffInfo);
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::runtime_error(str.c_str());
            }
        }
        catch(const std::exception & except)
        {
            qDebug() << except.what();
    }
}

void NetworkManager::get_show_info(MatchInfo &info, MatchInfo &buffInfo)
{
    QUrl url2(buffInfo.m_urlHomeData);
    QNetworkRequest request;
    request.setRawHeader("Host", "www.whoscored.com");
    request.setRawHeader("Connection", "keep-alive");
    request.setRawHeader("Accept", "text/plain, */*; q=0.01");
    request.setRawHeader("Model-last-Mode", m_paramMod2.toStdString().c_str());
    request.setRawHeader("X-Requested-With", "XMLHttpRequest");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");

    request.setRawHeader("Referer", buffInfo.m_urlShow.toStdString().c_str());
    request.setRawHeader("Accept-Encoding", "gzip, deflate, br");
    if(!m_cookies.isEmpty())
        m_jar->setCookiesFromUrl(m_cookies, url2);
    request.setUrl(url2);
    wait(1000);
    m_reply = m_manager->get(request);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result3;


    QString result;

        try
        {
            if(m_reply->error() == QNetworkReply::NoError)
            {
                QByteArray strBuf = m_reply->readAll();
                QByteArray result2;

                if(QCompressor::gzipDecompress(strBuf, result2))
                {
                    qDebug() << "Decompressed text is: " ;
                }
                else
                    qDebug() << "Can't decompress";
                result3 = result2;
                parser_match(result3, info);
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::runtime_error(str.c_str());
            }
        }
        catch(const std::exception & except)
        {
            qDebug() << except.what();
    }
}

void NetworkManager::get_show_info(MatchInfo & info)
{
    QUrl url2(info.m_urlHomeData);
    QNetworkRequest request;
    request.setRawHeader("Host", "www.whoscored.com");
    request.setRawHeader("Connection", "keep-alive");
    request.setRawHeader("Accept", "text/plain, */*; q=0.01");
    request.setRawHeader("Model-last-Mode", m_paramMod2.toStdString().c_str());
    request.setRawHeader("X-Requested-With", "XMLHttpRequest");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");

    request.setRawHeader("Referer", info.m_urlShow.toStdString().c_str());
    request.setRawHeader("Accept-Encoding", "gzip, deflate, br");
    if(!m_cookies.isEmpty())
        m_jar->setCookiesFromUrl(m_cookies, url2);
    request.setUrl(url2);
    wait(1000);
    m_reply = m_manager->get(request);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result3;


    QString result;

        try
        {
            if(m_reply->error() == QNetworkReply::NoError)
            {
                QByteArray strBuf = m_reply->readAll();
                QByteArray result2;

                if(QCompressor::gzipDecompress(strBuf, result2))
                {
                    qDebug() << "Decompressed text is: " ;
                }
                else
                    qDebug() << "Can't decompress";
                result3 = result2;
                parser_match(result3, info);
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::runtime_error(str.c_str());
            }
        }
        catch(const std::exception & except)
        {
            qDebug() << except.what();
    }
}

void NetworkManager::parser_match(QString & buff)
{
    buff = buff.replace("[[", "[").replace("]]", "]").replace("]", "").replace("'", "");
    QStringList listMatch = buff.split("[", QString::SkipEmptyParts);
    for(int i = 0; i < listMatch.count(); ++i)
    {
        MatchInfo infoBuf;
        QString buff2 = listMatch.at(i);
        QStringList listBuff = buff2.split(",", QString::SkipEmptyParts);
        infoBuf.m_idMatch = listBuff.at(0);
        infoBuf.m_nameFirstTeam = listBuff.at(6);
        infoBuf.m_nameSecondTeam = listBuff.at(9);
        infoBuf.m_countryName = listBuff.at(23);
        infoBuf.m_date = listBuff.at(2) + " " + listBuff.at(3);
        infoBuf.m_resultMatchFullTime = listBuff.at(11);
        infoBuf.m_ligaName = listBuff.at(22);
        infoBuf.m_year = listBuff.at(21);
        infoBuf.m_year2 = infoBuf.m_year.mid(infoBuf.m_year.indexOf("/") + 1);
        infoBuf.m_idFirstTeam =listBuff.at(8);
        infoBuf.m_idForNextUrl = listBuff.at(5);
        infoBuf.m_date2 = listBuff.at(3);
        if(infoBuf.m_date2[0] == ' ')
            infoBuf.m_date2 = infoBuf.m_date2.remove(0, 1);
        infoBuf.m_urlLive = "https://www.whoscored.com/Matches/" + infoBuf.m_idMatch + "/Live/" + infoBuf.m_countryName.replace(" ", "-") + "-" + infoBuf.m_ligaName.replace(" ", "-") + "-" + infoBuf.m_year.replace("/", "-") + "-" + m_current_home_away_url;
        auto locale = QLocale(QLocale::English, QLocale::UnitedStates);

        infoBuf.m_urlHomeData = "https://www.whoscored.com/teamsfeed/" + infoBuf.m_idForNextUrl  +"/PreviousMeetings/?awayTeamId=" + infoBuf.m_idFirstTeam + "&d=" +  locale.toString(locale.toDate( infoBuf.m_date2, "MMM d yyyy"), "yyyyMMdd")+ "&type=1";
        infoBuf.m_urlShow = "https://www.whoscored.com/Matches/" + infoBuf.m_idMatch + "/Show/" + infoBuf.m_countryName.replace(" ", "-") + "-" + infoBuf.m_ligaName.replace(" ", "-") + "-" + infoBuf.m_year.replace("/", "-") + "-" + m_current_home_away_url;
// 26-05-2013
        infoBuf.m_date2 = locale.toString(locale.toDate(infoBuf.m_date2, "MMM d yyyy"), "dd-MM-yyyy");
        m_allMatchs.push_back(infoBuf);
    }
    qDebug() << "Write ALl;";

}

void NetworkManager::parser_match(QString & buff, MatchInfo & info)
{
    buff = buff.replace("[[", "[").replace("]]", "]").replace("]", "").replace("'", "");
    QStringList listMatch = buff.split("[", QString::SkipEmptyParts);
    int countNormYear = 0;
    QFile file("Resylt.txt");
    file.open(QIODevice::WriteOnly);
    file.write(buff.toStdString().c_str());
    file.close();

    for(int i = 0; i < listMatch.count(); ++i)
    {
        MatchInfo infoBuf;
        QString buff2 = listMatch.at(i);
        QStringList listBuff = buff2.split(",");
        qDebug() << listBuff.length();
        infoBuf.m_idMatch = listBuff.at(0);
        infoBuf.m_nameFirstTeam = listBuff.at(5);
        infoBuf.m_nameSecondTeam = listBuff.at(8);
        infoBuf.m_countryName = listBuff.last();
        infoBuf.m_countryName = infoBuf.m_countryName.replace("\r\n", "");
        infoBuf.m_date = listBuff.at(2);
        infoBuf.m_resultMatchFullTime = listBuff.at(10);
        infoBuf.m_ligaName = listBuff.at(16);
        infoBuf.m_year = listBuff.at(15);
        QString ligaName = infoBuf.m_ligaName.replace("-", " ");
        infoBuf.m_idFirstTeam =listBuff.at(7);
        infoBuf.m_idForNextUrl = listBuff.at(4);
        QString m_datA =  listBuff.at(2);

        if(m_datA[0]==' ')
            m_datA.remove(0, 1);

        infoBuf.m_urlLive = "https://www.whoscored.com/Matches/" + infoBuf.m_idMatch + "/Live/" + infoBuf.m_countryName.replace(" ", "-") + "-" + infoBuf.m_ligaName.replace(" ", "-") + "-" + infoBuf.m_year.replace("/", "-") + "-" + m_current_home_away_url;
        auto locale = QLocale(QLocale::English, QLocale::UnitedStates);

        infoBuf.m_urlHomeData = "https://www.whoscored.com/teamsfeed/" + infoBuf.m_idForNextUrl  +"/PreviousMeetings/?awayTeamId=" + infoBuf.m_idFirstTeam + "&d=" + locale.toString(locale.toDate( m_datA, "dd-MM-yyyy"), "yyyyMMdd") + "&type=1";
        infoBuf.m_urlShow = "https://www.whoscored.com/Matches/" + infoBuf.m_idMatch + "/Show/" + infoBuf.m_countryName.replace(" ", "-") + "-" + infoBuf.m_ligaName.replace(" ", "-") + "-" + infoBuf.m_year.replace("/", "-") + "-" + m_current_home_away_url;
        infoBuf.m_elap =  listBuff.at(26);
        bool flagYear = false;
        for(int i = info.m_year2.toInt()-8; i <= info.m_year2.toInt(); ++i)
        {
          if(infoBuf.m_year.indexOf(QString::number(i)) != -1)
              flagYear = true;
        }
        if(readLeageElap(ligaName , infoBuf.m_elap) && flagYear && m_datA != info.m_date2)
            info.m_allShows.push_back(infoBuf);
        if(!flagYear)
            countNormYear++;

        qDebug()  << infoBuf.m_urlLive;
        qDebug()  << infoBuf.m_urlShow;

    }

    if(info.m_allShows.length() < 8 && listMatch.length() >= 6 && countNormYear == 0)
    {
        if(!info.m_allShows.isEmpty())
        parsing_match_show_page_2(info, info.m_allShows[info.m_allShows.length()-1]);
    }
}

void NetworkManager::parse_year_from_url(int &year1, int &year2, const QString &url)
{
    year1 = 0;
    year2 = 0;

    auto match = m_regex.globalMatch(url);

    if (!match.hasNext()) {
        qWarning() << "[NetworkManager] Years not found in url:" << url;
        return;
    }

    year1 = match.next().captured(1).toInt();

    if (match.hasNext()) {
        year2 = match.next().captured(1).toInt();
    }
}
