#ifndef LOGGER_H
#define LOGGER_H

#include <QtCore>

/**
 * @brief The Logger class - функционал для логгирования
 */
class Logger
{
public:
    Logger();

public:
    /**
     * @brief log_to_file - логгирование в файл
     * @param message - сообщение лога
     * @param path - путь к файлу
     */
    static void log_to_file(const QString & message, const QString & path = "log.txt");
};

#endif // LOGGER_H
