#ifndef MATCH_H
#define MATCH_H

#include <QMap>
#include <QSet>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QFile>

/**
 * @brief The Match class - класс главного матча
 */
class Match
{
public:
    Match();

public:

    /**
     * @brief The HelperData struct - вспомогательные данные
     */
    struct HelperData {
        // записан ли сезон в формате "год"
        bool is_single_season = true;

        QString show_url;
        QString home_vs_away_url;
        QString homeaway_url;
    };

    // ссылка на матч
    QString m_url;

    // список хед_ту_хед матчей
    QList <Match> m_submatch_list;

    // вспомогательные данные
    HelperData m_helper_data;

    /**
     * @brief The ThreeMinutes struct - 3х-минутные данные
     */
    struct ThreeMinutes {
        // до 75й минуты
        int before_75 = 0;
        // до 42й минуты
        int before_42 = 0;
        // до 21й минуты
        int before_21 = 0;
    };

    /**
     * @brief The Event struct - событие матча
     */
    struct Event {
        /**
         * @brief The EventType enum - типы событий
         */
        enum EventType { Shot, Goal, Card, Unknow };

        /**
         * @brief The ShotCategory enum - категории ударов
         */
        enum ShotCategory { Zero, First, Second, Third };

        // json-данные события
        QJsonObject json;

        // id события
        int id;

        // id команды
        int team_id;

        // минута события
        int minute = 0;

        // expanded-минута события
        int minute_expanded = 0;

        // тайм события
        int period = 0;

        // заблокированный
        bool is_blocked;

        // bigchance
        bool is_bigchance;

        // удар ногой
        bool is_footed;

        // в пределах зоны
        bool is_not_out_of_box;

        // автогол
        bool is_owngoal;

        // тип события - по умолчанию неизвестный
        EventType type = Unknow;

        // список категорий, к которому относится событие
        QSet <ShotCategory> category_list;

        // список id типов, под которые попадает событие
        QSet <int> type_id_list;

        // конструктор по-умолчанию
        Event() : is_blocked(false), is_bigchance(false), is_footed(false), is_not_out_of_box(false), is_owngoal(false) {}
        // конструктор с инициализацией полей
        Event(bool blocked, bool bigchance, bool footed, bool inbox) :
            is_blocked(blocked), is_bigchance(bigchance), is_footed(footed), is_not_out_of_box(inbox) {}
    };

    /**
     * @brief EventList - тип Список событий в формате id-обытие
     */
    typedef QMultiMap <int, Event> EventList;

    /**
     * @brief The Team struct - Команда
     */
    struct Team {
        // json-данные команды
        QJsonObject json;

        // id клманды
        int id;

        // список событий команды
        EventList event_list;

        // список пассов команды
        QMap <int, int> pass_list;

        // данные о пассах по минутам
        ThreeMinutes pass;

        // счет команды
        int score = 0;

        // список голов команды
        QList <int> goal_minute_list;

        // минута первой красной карточки
        int first_red_minute = -1;

        // минута второй красной карточки
        int second_red_minute = -1;

        // расстановка команды
        QString aligment;

        // название команды
        QString name;

        // данные незаблокированных или bigchance и заблокированных ударов
        ThreeMinutes noblocked_or_bigchance_and_blocked;

        // данные bigchance ударов
        ThreeMinutes bigchance;

        // данные незаблокированных или bigchance и заблокированных ударов,
        // сделанных ногой в пределах зоны
        ThreeMinutes noblocked_or_bigchance_and_blocked_left_or_right_from_penalty;
    };

    /**
     * @brief The Data struct - данные матча
     */
    struct Data {
        // сезон матча
        QString season;

        // название чемпионата
        QString name_championship;

        // время матча
        QString time_match;

        // дата матча
        QString date_match;

        // код погоды во время матча
        int weather = 0;

        // матч плейоффа
        bool is_playoff = false;

        // played
        int played = 0;

        // указатель на первую команду
        Team * team1;

        // указатель на вторую команду
        Team * team2;
    };

    /**
     * @brief The PeriodMinuteLimit struct - минуты завершения таймов
     */
    struct PeriodMinuteLimit {
        // минута завершения первого тайма
        int first_period = 0;

        // минута завершения второго тайма
        int second_period = 0;
    };

    /**
     * @brief BracketIndexList - список номеров скобок для InitialData-массива
     * в формате номер пары скобок - индекс левой скобки - индекс правой скобки
     */
    typedef QMap <int, QPair <int, int> > BracketIndexList;

private:

    /** @brief m_data_raw - данные матча в формате "как есть" */
    QString m_data_raw;

    // json-данные всего матча
    QJsonObject m_doc;

    /**
     * @brief TeamDictionary - словарь команд в формате номер команды - id команды
     */
    typedef QMap <int, int> TeamDictionary;
    TeamDictionary m_team_dictionary;

    /**
     * @brief TeamList - список команд в формате номер команды - команда
     */
    typedef QMap <int, Team> TeamList;
    TeamList m_team_list;

    /**
     * @brief ExpandedMinuteList - список expanded-минут матча
     */
    typedef QMap <int, QMap <int, int> > ExpandedMinuteList;
    ExpandedMinuteList m_expanded_minute_list;

    /**
     * @brief m_period_minute_limit - минуты завершения таймов
     */
    PeriodMinuteLimit m_period_minute_limit;

    // данные матча
    Data m_data;

    // список уникальных значений для отсеивания событий по минутам
    QSet <int> m_set;

signals:

public:
    /**
     * @brief load - спарсить матч
     * @param path - путь к файлу или исходные данные
     * @param from_file - парсить ли из файла
     * @return true, если парсинг матча прошел успешно
     */
    bool load(const QString & path, bool from_file = false);

    /**
     * @brief get_shot_category - присвоить событию возможные категории
     * @param event - событие
     */
    static void get_shot_category(Event & event);

    /**
     * @brief get_bracket_index_list - вычислить список индексов скобок
     * @param raw - исходные данные - массив InitialData
     * @return пронумерованный список индексов скобок
     */
    static BracketIndexList get_bracket_index_list(const QString & raw);

    /**
     * @brief get_node_list - список объектов InitialData массива
     * @param raw - InitialData массив
     * @return список объектов
     */
    static QList <QString> get_node_list(const QString & raw);

    /**
     * @brief get_regex_parsed_header - спарсить сезон матча через регулярки
     * @param header_raw заголовок матча с сезоном
     * @return совпадение от регулярки
     */
    QRegularExpressionMatch get_regex_parsed_header(const QString & header_raw);

    /**
     * @brief get_data - получить текущие данные по матчу
     * @return данные матча
     */
    Data get_data() const { return m_data; }

    /**
     * @brief append_submatch - добавить матч в список хед_ту_хед матчей
     * @param match - хед_ту_хед матч
     */
    void append_submatch(Match &match);

    /**
     * @brief set_empty - сделать текущий матч пустым
     */
    void set_empty() { post_parse(true); }

    /**
     * @brief set_empty - сделать текущий матч пустым и установить год матча
     * @param year - год матча
     * @param is_single_season - сезон матча с помощью одного года
     */
    void set_empty(int year, bool is_single_season);

    /**
     * @brief parse_played - спарсить значение played
     */
    void parse_played();

private:
    /**
     * @brief parse - спарсить матч по json-данным
     */
    void parse();

    /**
     * @brief parse_initial_array - спарсить матч по InitialData массиву
     * @param raw - InitialData массив в виде "как есть"
     */
    void parse_initial_array(const QString &raw);

    /**
     * @brief parse_score_from_initial_array - спарсить счет с InitialData массива
     * @param raw - исходные данные
     */
    void parse_score_from_initial_array(const QString &raw);

    /**
     * @brief parse_expanded_minute_list - спарсить список expanded-минут
     */
    void parse_expanded_minute_list();

    /**
     * @brief parse_period_minute_limit - спарсить данные о завершении таймов
     */
    void parse_period_minute_limit();

    /**
     * @brief parse_event - спарсить событие
     * @param json - исходные данные события в json
     * @return событие
     */
    Event parse_event(const QJsonObject & json);

    /**
     * @brief parse_team - спарсить команду
     * @param json - исходные данные команды в json
     * @return команда
     */
    Team parse_team(const QJsonObject & json);

    /**
     * @brief parse_team_pass - спарсить данные о пассах конкретной команды
     * @param team - команда
     */
    void parse_team_pass(Team & team);

    /**
     * @brief filter_event_list - отсеять список событий команды
     * @param team_number - номер команды (1 или 2)
     */
    void filter_event_list(int team_number);

    /**
     * @brief filter_event - отсеять конкретное событие
     * @param team - команда события
     * @param event - событие
     */
    void filter_event(Team * team, Event & event);

    /**
     * @brief filter_shot - распределить удар по минутам
     * @param categorys - 3х-минутные данные
     * @param minute - минута удара
     */
    void filter_shot(ThreeMinutes & categorys, int minute);

    /**
     * @brief parse_season_and_championship - спарсить сезон матча и название чемпионата
     */
    void parse_season_and_championship();

    /**
     * @brief parse_datetime - спарсить дату матча
     */
    void parse_datetime();

    /**
     * @brief post_parse - действия, после окончания парсинга (приведение данных в надлежащий вид)
     * @param is_empty - пустой ли матч
     */
    void post_parse(bool is_empty = false);

    /**
     * @brief expanded_minute - получить expanded-минуту по обычной минуте
     * @param minute - обычная минута
     * @param period - номер тайма матча
     * @param key_list - список минут тайма
     * @return expanded-минута согласно переданным данным
     */
    int expanded_minute(int minute, int period, QList<int> & key_list);

    /**
     * @brief set_to_zero_three_minute - заполнить 3хминутные данные нулями
     * @param minutes 3х-минутные данные
     */
    void set_to_zero_three_minute(ThreeMinutes & minutes);

    /**
     * @brief set_to_zero_team - заполнить все данные команды нулями
     * @param team - команда
     */
    void set_to_zero_team(Team * team);

};

#endif // MATCH_H
