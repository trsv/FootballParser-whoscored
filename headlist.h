#ifndef HEADLIST_H
#define HEADLIST_H

#include "struct.h"

#include <QMultiMap>
#include <QJsonObject>

/**
 * @brief The HeadList class - список хед_ту_хед матчей
 */
class HeadList
{
public:
    HeadList();

public:
    /**
     * @brief set_current_season - проинициализировать значение сезона главного матча
     * @param season - сезон главного матча (год или год/год)
     */
    void set_current_season(const QString &season);

    /**
     * @brief get_list - возвращает текущий список хед_ту_хед матчей
     * @return список матчей в формате год-матч
     */
    QMultiMap <int, MatchInfo> get_list() const;


    /**
     * @brief add - добавить хед_ту_хед матч в список
     * @param value - матч
     * @return true - если успешно добавили, иначе false
     */
    bool add(const MatchInfo & value);

    /**
     * @brief contains - проверка на наличие матча по ключу (год матча)
     * @param key - год матча
     * @return true, если матч с таким годом существует
     */
    bool contains(const int &key) const;

    /**
     * @brief contains - проверка на наличие матча по ключу и значению
     * @param key - год матча
     * @param info - сам матч
     * @return true, если матч с таким годом есть в списке
     */
    bool contains(const int &key, const MatchInfo & info) const;

    /**
     * @brief fill_empty - дополнить список пустыми матчами
     */
    void fill_empty();

private:
    /**
     * @brief get_last_skipped_year - получить год последнего пропущеного матча
     * @return возвращает год
     */
    int get_last_skipped_year();

private:
    /** @brief m_list - список матчей в формате год-матч */
    QMultiMap <int, MatchInfo> m_list;

    /** @brief m_current_season - сезон текущего матча */
    int m_current_season = 0;

    /** @brief m_first_season - первый сезон матча из 8ми */
    int m_first_season = 0;

    /** @brief m_regex - регулярка для парсинга годов из строки сезона */
    const QRegularExpression m_regex;
};

#endif // HEADLIST_H
