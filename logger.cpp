#include "logger.h"

#include <QFile>

Logger::Logger() {}

void Logger::log_to_file(const QString &message, const QString &path)
{
    // файл лога
    QFile log_file(path);

    // если открыли файл, то пишем лог туда
    if (log_file.open(QIODevice::WriteOnly | QIODevice::Append)) {

        log_file.write((message + "\r\n").toStdString().c_str());
        log_file.close();

    } else {

        qWarning() << "[LOGGER] Can`t log to file:" << path;
        return;

    }
}
