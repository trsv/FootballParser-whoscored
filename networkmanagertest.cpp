#include "networkmanagertest.h"

NetworkManagerTest::NetworkManagerTest(QObject *parent) :
    QObject(parent)
{

}

void NetworkManagerTest::parse_year_test()
{
    struct Years {

        Years(int y1, int y2) : year1(y1), year2(y2) {}

        int year1 = 0;
        int year2 = 0;
    };

    QMap <QString, Years> m_test_list;

    m_test_list.insert("https://www.whoscored.com/Regions/74/Tournaments/22/Seasons/4279/Stages/9105/Show/France-Ligue-1-2014-2015",
                       Years(2014, 2015));
    m_test_list.insert("https://www.whoscored.com/Regions/233/Tournaments/85/Seasons/5607/Stages/11890/Show/USA-Major-League-Soccer-2015-2016",
                       Years(2015, 2016));
    m_test_list.insert("https://www.whoscored.com/Regions/233/Tournaments/85/Seasons/5607/Stages/11890/Show/USA-Major-League-Soccer-2015",
                       Years(2015, 0));

    for (auto it = m_test_list.begin(); it != m_test_list.end(); ++it) {
        int year1 = 0;
        int year2 = 0;

        m_manager.parse_year_from_url(year1, year2, it.key());

        QVERIFY2(year1 == it.value().year1, it.key().toStdString().c_str());
        QVERIFY2(year2 == it.value().year2, it.key().toStdString().c_str());
    }

}

void NetworkManagerTest::parse_home_year_away_test()
{
    QMap <QString, QString> url_list;
    url_list.insert("https://www.whoscored.com/Matches/1173280/Live/China-Super-league-2017-Chongqing-Lifan-Guangzhou-R-F-F-C-",
                    "Chongqing-Lifan-Guangzhou-R-F-F-C-");
    url_list.insert("https://www.whoscored.com/Matches/1173127/Live/China-Super-league-2017-Guizhou-Hengfeng-Liaoning-Hongyun",
                    "Guizhou-Hengfeng-Liaoning-Hongyun");

    for (auto it = url_list.begin(); it != url_list.end(); ++it) {
        auto url_valid = it.value();
        auto url_test = HtmlDecoder::parse_homeaway_url(it.key());

        QVERIFY2(url_test == url_valid, url_test.toStdString().c_str());
    }
}
