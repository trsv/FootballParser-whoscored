#include "headlisttest.h"

HeadListTest::HeadListTest(QObject *parent)
{

}

void HeadListTest::builder_test()
{
    // год, с которым будем создавать объект
    int year = 1999;

    // тестируемый объект
    auto test_info = MatchInfoBuilder().year(year)->build();

    // совпадает ли год созданного объекта с нашим
    QCOMPARE(test_info.m_year.toInt(), year);
}

void HeadListTest::unique_test()
{
    // список хед_ту_хед матчей
    HeadList list;

    // инициализируем сезон матча
    list.set_current_season("2012/2013");

    // создаем список объектов матчей
    QList <MatchInfo> info_list;
    for (int year = 1960; year < 2050; year++) {
        info_list << MatchInfoBuilder().year(year)->build();
    }

    // добавляем все в список
    for (auto info : info_list) {
        list.add(info);
    }

    // созданный список
    auto prepared_year_list = list.get_list().keys();

    // выводим года матчей созданого списка
    for (auto year : prepared_year_list) {
        qDebug() << year;
    }

    // правильный список годов
    QList <int> valid_year_list;
    for (int year = 2004; year < 2012; year++) {
        valid_year_list << year;
    }

    // выводим кол-во матчей внутри созданого списка
    qDebug() << "total count:" << prepared_year_list.size();

    // проверяем созданый и правильный списки
    QCOMPARE(prepared_year_list, valid_year_list);

    // проверяем, чтоб кол-во равнялось 8ми
    QVERIFY(prepared_year_list.size() == 8);
}

void HeadListTest::multi_test()
{
    HeadList list;

    list.set_current_season("2012/2013");

    QList <MatchInfo> info_list;
    for (int year = 2050; year > 1950; year--) {

        for (int multi = 0; multi < 2; multi++) {
            info_list << MatchInfoBuilder().year(year)->build();
        }

    }

    for (auto info : info_list) {
        list.add(info);
    }

    auto prepared_year_list = list.get_list().keys();
    for (auto year : prepared_year_list) {
        qDebug() << year;
    }

    QList <int> valid_year_list;
    for (int year = 2008; year < 2012; year++) {

        for (int multi = 0; multi < 2; multi++) {
            valid_year_list << year;
        }

    }

    qDebug() << "total count:" << prepared_year_list.size();
    QCOMPARE(prepared_year_list, valid_year_list);
    QVERIFY(prepared_year_list.size() == 8);
}

void HeadListTest::empty_test()
{
    HeadList list;

    list.set_current_season("2012/2013");

    QList <MatchInfo> info_list;
    for (int year = 2012; year > 2009; year--) {
        info_list << MatchInfoBuilder().year(year)->build();
    }

    for (auto info : info_list) {
        list.add(info);
    }

    list.fill_empty();

    auto prepared_year_list = list.get_list().keys();
    for (auto year : prepared_year_list) {
        qDebug() << year;
    }

    QList <int> valid_year_list;
    for (int year = 2004; year < 2012; year++) {
        valid_year_list << year;
    }

    qDebug() << "total count:" << prepared_year_list.size();
    QCOMPARE(prepared_year_list, valid_year_list);
    QVERIFY(prepared_year_list.size() == 8);
}

void HeadListTest::empty_list_test()
{
    HeadList list;
    list.set_current_season("2012/2013");

    list.fill_empty();

    auto prepared_list = list.get_list();
    for (auto year : prepared_list.keys()) {
        qDebug() << year;
    }

    QVERIFY(prepared_list.size() == 8);
}

void HeadListTest::real_test()
{
    HeadList list;

    list.set_current_season("2017/2018");

    QList <MatchInfo> info_list;
    info_list << MatchInfoBuilder().year(2010)->build();
    info_list << MatchInfoBuilder().year(2011)->build();
    info_list << MatchInfoBuilder().year(2013)->build();
    info_list << MatchInfoBuilder().year(2014)->build();

    for (auto info : info_list) {
        list.add(info);
    }

    list.fill_empty();

    auto prepared_year_list = list.get_list().keys();
    for (auto year : prepared_year_list) {
        qDebug() << year;
    }

    QList <int> valid_year_list;
    for (int year = 2009; year < 2017; year++) {
        valid_year_list << year;
    }

    qDebug() << "total count:" << prepared_year_list.size();
    QCOMPARE(prepared_year_list, valid_year_list);
    QVERIFY(prepared_year_list.size() == 8);
}
