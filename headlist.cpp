#include "headlist.h"

HeadList::HeadList() :
    m_regex("^\\d+")
{

}

void HeadList::set_current_season(const QString &season)
{
    // вычисление текущего сезона
    m_current_season = m_regex.match(season).captured(0).toInt();

    // вычисление года первого матча в списке
    m_first_season = m_current_season - 8;
}

bool HeadList::add(const MatchInfo &value)
{
    // не добавляем матч, если в списке уже 8 матчей
    if (m_list.size() >= 8) {
        return false;
    }

    // год матча
    int year = m_regex.match(value.m_year).captured(0).toInt();

    // если год матча не спарсился (матч пуст или ошибка парсинга)
    if (year == 0) {

        // то вычисляем последний пропущенный матч
        year = get_last_skipped_year();

    } else {

        // иначе, проверяем наличие этого матча в списке
        // если он уже есть (одинаковый сезон и ссылка на матч), то не добавляем
        if (contains(year, value) && !value.m_urlLive.isEmpty()) return false;
    }

    // если год подходит, добавляем в список для выгрузки
    if (year >= m_first_season && year < m_current_season) {
        m_list.insert(year, value);
        return true;
    } else {
        return false;
    }
}

bool HeadList::contains(const int & key) const
{
    // возвращаем наличие ключа в списке
    return m_list.contains(key);
}

bool HeadList::contains(const int &key, const MatchInfo &info) const
{
    // если нет ключа, то и матча нет в списке
    if (!contains(key)) return false;

    // проверяем все матчи с этим ключом
    for (auto value : m_list.values(key)) {

        // если ссылки на матчи совпали, значит такой матч есть в списке
        if (info.m_urlLive.toLower() == value.m_urlLive.toLower()) {
            return true;
        }
    }

    // нет ни одного совпадения
    return false;
}

void HeadList::fill_empty()
{
    // рекурсивно заполняем список до 8ми объектов
    if (add(MatchInfo())) {
        fill_empty();
    } else {
        return;
    }
}

int HeadList::get_last_skipped_year()
{
    // будем двигаться от позднего матча к раннему
    // пусть пропущеный год равен текущему сезону
    int skipped_year = m_current_season;

    // уменьшаем год, пока такой есть в списке и вписывается в рамки
    do {
        --skipped_year;
    } while (m_list.contains(skipped_year) && skipped_year > m_first_season);

    // возвращаем вычисленный пропущеный год
    return skipped_year;
}

QMultiMap<int, MatchInfo> HeadList::get_list() const
{
    // возвращаем текущий список
    return m_list;
}
