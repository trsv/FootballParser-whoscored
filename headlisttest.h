#ifndef HEADLISTTEST_H
#define HEADLISTTEST_H

#include "headlist.h"

#include <QObject>
#include <QtTest/QtTest>

/**
 * @brief The HeadListTest class - набор тестов для списка хед_ту_хед матчей
 */
class HeadListTest : public QObject
{
    Q_OBJECT

public:
    explicit HeadListTest(QObject * parent = nullptr);

private slots:
    /**
     * @brief builder_test - тест сборщика объекта матча
     */
    void builder_test();

    /**
     * @brief unique_test - тест списка матчей с уникальными годами
     */
    void unique_test();

    /**
     * @brief multi_test - тест списка матчей с повторяющимися годами
     */
    void multi_test();

    /**
     * @brief empty_test - тест списка матчей с пропущенными годами
     */
    void empty_test();

    /**
     * @brief empty_list_test - тест пустого списка матчей
     */
    void empty_list_test();

    /**
     * @brief real_test - тест с реальными значениями
     */
    void real_test();

private:

    /**
     * @brief The MatchInfoBuilder class - сборщик объектов матчей
     */
    class MatchInfoBuilder {
    private:
        /** @brief m_year - год матча */
        int m_year;

    public:
        /**
         * @brief build - создать объект матча
         * @return возвращает объект матча
         */
        MatchInfo build() {
            MatchInfo info;
            info.m_year = QString::number(m_year);
            return info;
        }

        /**
         * @brief year - инициализация года матча
         * @param year - год матча
         * @return указатель на текущий объект сборщика
         */
        MatchInfoBuilder* year(const int & year) { m_year = year; return this; }
    };
};

#endif // HEADLISTTEST_H
