#ifndef IO_H
#define IO_H

#include "match.h"

#include <QObject>

/**
 * @brief The IO class - ввод/вывод в файл
 */
class IO : public QObject
{
    Q_OBJECT
public:
    explicit IO(QObject *parent = nullptr);

signals:

public slots:
    /**
     * @brief save_to_json - записать матч в json-формате
     * @param match - матч для записи
     * @param path - путь к файлу
     */
    void save_to_json(const Match & match, const QString & path);

    /**
     * @brief convert_to_json - конвертировать матч в json-объект
     * @param match - исходный матч
     * @param is_submatch - флаг, является ли данный матч хед_ту_хед матчем
     * @return json-объект текущего матча
     */
    QJsonObject convert_to_json(const Match & match, bool is_submatch = false);

private slots:
    /**
     * @brief prepare_team - конвертировать данные команды в json-объект
     * @param team - команда
     * @param only_goals - флаг для конвертации только данных по голам
     * @return json-объект команды
     */
    QJsonObject prepare_team(Match::Team * team, bool only_goals = false);

    /**
     * @brief prepare_team_goal - записать список голов команды в json
     * @param team - команда
     * @return json-объект со списком голов
     */
    QJsonObject prepare_team_goal(Match::Team * team);

    /**
     * @brief prepare_team_card - записать список карточек в json
     * @param team - команда
     * @return json-объект со списком карт
     */
    QJsonObject prepare_team_card(Match::Team * team);

    /**
     * @brief prepare_team_three_minute - записать 3х-минутные данные в json
     * @param minutes - объект с данными
     * @return json-объект с данными
     */
    QJsonObject prepare_team_three_minute(Match::ThreeMinutes &minutes);

};

#endif // IO_H
